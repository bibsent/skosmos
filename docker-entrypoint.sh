#!/usr/bin/env bash
set -euo pipefail

CONFIG_FILE=/etc/apache2/sites-available/000-default.conf

SERVER_NAME=${SERVER_NAME:-localhost}
SERVER_NAME_ID_SERVICE=${SERVER_NAME_ID_SERVICE:-localhost}
APP_ENV=${APP_ENV:-prod}
SCRIPT_DIR="$(dirname "$BASH_SOURCE")"

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo "Configuring and starting Apache"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo ":: SCRIPT_DIR: $SCRIPT_DIR"
echo ":: Config file: $CONFIG_FILE"
echo ":: Skosmos host: $SERVER_NAME"
echo ":: ID service host: $SERVER_NAME"
echo ":: APP_ENV: $APP_ENV"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

# Avoid host header attacks by using a fixed server name
sed -ri -e "s/\{\{SERVER_NAME\}\}/${SERVER_NAME}/g" $CONFIG_FILE
sed -ri -e "s/\{\{SERVER_NAME_ID_SERVICE\}\}/${SERVER_NAME_ID_SERVICE}/g" $CONFIG_FILE

# Set server admin
if [[ -n "${SERVER_ADMIN+x}" ]]; then
    echo "Server admin: $SERVER_ADMIN"
    sed -ri -e "s/^(\s*)#? ?ServerAdmin .*/\1ServerAdmin ${SERVER_ADMIN}/g" $CONFIG_FILE
fi

# PHP settings
if [[ "$APP_ENV" == "prod" ]]; then
    cp "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
    cp "$SCRIPT_DIR/opcache.ini" "$PHP_INI_DIR/conf.d/"
else
    cp "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"
fi
sed -i "s/;date.timezone =.*/date.timezone = UTC/" "$PHP_INI_DIR/php.ini" \
    && sed -i "s/memory_limit = 128M/memory_limit = 348M/" "$PHP_INI_DIR/php.ini" \
    && sed -i "s/file_uploads = On/file_uploads = Off/" "$PHP_INI_DIR/php.ini"

# Configure mail
# Reference: https://marlam.de/msmtp/msmtp.html
cat <<EOF >/etc/msmtprc
account default
host $SMTP_HOST
port $SMTP_PORT
domain $SMTP_DOMAIN
tls on
tls_starttls on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
tls_certcheck on
auth on
user $SMTP_USER
password "$SMTP_PASSWORD"
from "$SMTP_SENDER"
logfile -
EOF

echo Starting server
exec docker-php-entrypoint "$@"
