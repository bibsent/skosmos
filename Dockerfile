FROM php:7.4-apache

ARG COMPOSER_AUTH

RUN apt-get update \
    && \
        apt-get -y install \
            locales \
            libicu-dev \
            libxslt-dev \
            zip \
            unzip \
            libzip-dev \
            zlib1g-dev \
            git \
            msmtp \
    && \
        for locale in en_GB en_US fi_FI fr_FR sv_SE nb_NO nn_NO; do \
            echo "${locale}.UTF-8 UTF-8" >> /etc/locale.gen ; \
        done \
    && \
        locale-gen \
    && rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite

RUN docker-php-ext-configure opcache --enable-opcache \
    && \
        docker-php-ext-install \
            opcache \
            gettext \
            intl \
            xsl \
            zip

# Enable modproxy for the id service
RUN ln -s /etc/apache2/mods-available/proxy.load /etc/apache2/mods-enabled/proxy.load \
    && ln -s /etc/apache2/mods-available/proxy_http.load /etc/apache2/mods-enabled/proxy_http.load \
    && ln -s /etc/apache2/mods-available/ssl.load /etc/apache2/mods-enabled/ssl.load

# Overwrite default Apache vhost config
COPY 000-default.conf /etc/apache2/sites-available/

# Configure PHP to use mSMTP
RUN echo 'sendmail_path = "/usr/bin/msmtp -t"' >> /usr/local/etc/php/conf.d/mail.ini

# Composer deps
COPY composer.json /var/www/html/
RUN curl -sS https://getcomposer.org/installer | php --
RUN mkdir -p /var/www/html/controller /var/www/html/model/sparql
RUN php composer.phar install --no-interaction --no-dev --no-suggest --optimize-autoloader --profile

COPY docker-entrypoint.sh opcache.ini /docker/

COPY . /var/www/html
RUN php composer.phar dump-autoload --optimize

ENTRYPOINT ["/docker/docker-entrypoint.sh"]

CMD ["apache2-foreground"]

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 CMD curl -sS --fail 'http://localhost/rest/v1/health' || exit 1
